import '../css/main.scss';
import '@babel/polyfill';

import { geoConicConformalSpain } from 'd3-composite-projections';
import { feature } from 'topojson-client';

//Variables a utilizar por los tres mapas
let mapWidth = document.getElementById('map_psoe').clientWidth, mapHeight = document.getElementById('map_psoe').clientHeight;
let poligonos, poligonosFeatures, path, projection;

initData();

function initData() {
    let q = d3.queue();

    q.defer(d3.json, 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_izquierda/municipios_distritos_madrid.json');
    q.defer(d3.csv, 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_izquierda/autonomicas_distribucion.csv');

    q.await(function(error, map, data) {
        if (error) throw error;

        poligonos = feature(map, map.objects.municipios_distritos_madrid);
        projection = geoConicConformalSpain().scale(2000).fitSize([mapWidth,mapHeight], poligonos);
        poligonosFeatures = [...poligonos.features];        
        path = d3.geoPath(projection);

        poligonosFeatures.forEach(function(item) {
            let innerData = data.filter(function(subItem) { if (item.properties.COD == subItem.COD) { return subItem; }})[0];
            item.properties.nombre = innerData.Municipio;
            item.properties.psoe = +innerData['%PSOE'];
            item.properties.podemos = +innerData['%Podemos'];
            item.properties.mas_madrid =  +innerData['%Más Madrid'];
        });

        //Desarrollo de cada mapa
        initPsoe();
        initMasMadrid();
        initPodemos();
    });
}

function initPsoe() {
    let svg = d3.select('#map_psoe').append("svg")
        .attr("width", mapWidth)
        .attr("height", mapHeight);

    let g = svg
        .append('g');

    g.selectAll("path")
        .data(poligonosFeatures)
        .enter()
        .append("path")
        .attr("d", path)
        .attr("class", 'region-mun')
        .style("fill", function(d){ 
            if(d.properties.psoe <= 5) {
                return '#f1f0ea';
            } else if (d.properties.psoe > 5 && d.properties.psoe <= 10) {
                return '#d8e2e4';
            } else if (d.properties.psoe > 10 && d.properties.pose <= 15) {
                return '#a8c5d7';
            } else if (d.properties.psoe > 15 && d.properties.psoe <= 20) {
                return '#708c9e';
            } else if (d.properties.psoe > 20 && d.properties.psoe <= 25) {
                return '#395365';
            } else {
                return '#021a2b';
            }
        })
        .style('opacity', 1)
        .style("stroke", "#fff")
        .style("stroke-width", "0.2px");
}

function initMasMadrid() {
    let svg = d3.select('#map_mas-madrid').append("svg")
        .attr("width", mapWidth)
        .attr("height", mapHeight);

    let g = svg
        .append('g');

    g.selectAll("path")
        .data(poligonosFeatures)
        .enter()
        .append("path")
        .attr("d", path)
        .attr("class", 'region-mun')
        .style("fill", function(d){ 
            if(d.properties.mas_madrid <= 5) {
                return '#f1f0ea';
            } else if (d.properties.mas_madrid > 5 && d.properties.mas_madrid <= 10) {
                return '#d8e2e4';
            } else if (d.properties.mas_madrid > 10 && d.properties.mas_madrid <= 15) {
                return '#a8c5d7';
            } else if (d.properties.mas_madrid > 15 && d.properties.mas_madrid <= 20) {
                return '#708c9e';
            } else if (d.properties.mas_madrid > 20 && d.properties.mas_madrid <= 25) {
                return '#395365';
            } else {
                return '#021a2b';
            }
        })
        .style('opacity', 1)
        .style("stroke", "#fff")
        .style("stroke-width", "0.2px");
}

function initPodemos() {
    let svg = d3.select('#map_podemos').append("svg")
        .attr("width", mapWidth)
        .attr("height", mapHeight);

    let g = svg
        .append('g');

    g.selectAll("path")
        .data(poligonosFeatures)
        .enter()
        .append("path")
        .attr("d", path)
        .attr("class", 'region-mun')
        .style("fill", function(d){ 
            if(d.properties.podemos <= 5) {
                return '#f1f0ea';
            } else if (d.properties.podemos > 5 && d.properties.podemos <= 10) {
                return '#d8e2e4';
            } else if (d.properties.podemos > 10 && d.properties.podemos <= 15) {
                return '#a8c5d7';
            } else if (d.properties.podemos > 15 && d.properties.podemos <= 20) {
                return '#708c9e';
            } else if (d.properties.podemos > 20 && d.properties.podemos <= 25) {
                return '#395365';
            } else {
                return '#021a2b';
            }
        })
        .style('opacity', 1)
        .style("stroke", "#fff")
        .style("stroke-width", "0.2px");
}